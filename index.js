const fetch = require('isomorphic-fetch');
const _ = require('lodash');
const express = require('express');
const app = express();

const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';
let pc = {};

fetch(pcUrl)
    .then((res) => {
        return res.json();
    })
    .then((pcData) => {
        pc = pcData;
    })
    .catch(err => {
        console.log('Что-то пошло не так:', err);
    });

app.options('/*', function (req, res) {
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Headers', 'Content-Type');
    res.status(200).send();
});

app.get('/task3a/*', function (req, res) {
    const path = req.params[0].replace(/[\.\[\]]/g, '').replace(/\//g, '.').replace(/\.$/, '');

    res.set('Access-Control-Allow-Origin', '*');

    if (path.length === 0) {
        res.json(pc);
    } else if (path === 'volumes') {
        let result = {};

        pc.hdd.forEach((hdd) => {
            result[hdd.volume] = result[hdd.volume] !== undefined ? result[hdd.volume] + hdd.size : hdd.size;
        });

        Object.keys(result).forEach(key => {
            result[key] = `${result[key]}B`;
        });

        res.json(result);
    } else {
        if (_.has(pc, path) && path.indexOf('.length') === -1) {
            res.json(_.get(pc, path));
        } else {
            res.status(404).send('Not Found');
        }
    }
});

app.listen(3000, function () {
    console.log('App listening on port 3000!');
});